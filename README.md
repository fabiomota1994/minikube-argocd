Minikube with Argo

1 - kubectl create namespace argocd

2 - kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

3 - kubectl get secret argocd-initial-admin-secret -n argocd -o yaml
to login in ARGO UI after port-forward

4 - kubectl apply -f C:\\\minikube-argocd-v2\minikube-argocd\templates\argoCd\templates\application.yaml

5 - kubectl port-forward svc/argocd-server -n argocd 8080:443

6 - minikube tunnel for forward ports to localhost

Minikube with Helms only

1 - helm install services dir of helms : example C:\\\minikube-argocd-v2\minikube-argocd\templates\services
if some value is change just do

2 - helm upgrade services dir of helm : C:\\\minikube-argocd-v2\minikube-argocd\templates\services
example of change: need of kafka put value create with true

3 - minikube tunnel for forward ports to localhost
( for kafka you must add to your pc hosts the name o the service kafka for this helms add two lines
    127.0.0.1 kafka
    127.0.0.1 kafka-instance-0.kafka.default.svc.cluster.local )